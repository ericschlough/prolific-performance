
![coverage](https://cmsc435.garrettvanhoy.com/gvanhoy/simple_flask_docker/badges/master/coverage.svg?style=flat-square)


# simple_flask_docker

A repository with a Dockerfile and docker-compose used to dockerize and a simple Flask app and a MySQL database using SQLAlchemy and WTForms.

## Commands run in class
```
docker run python:3.6 bash # runs bash inside a container with image python:3.6
docker container ls -la # lists all containers including stopped containers
docker run -it python:3.6 bash  # runs bash inside a container with image python:3.6 interactively
docker run -it -v /path/on/host/:/app python:3.6 bash # mounts host directory into container to be interacted with
docker run -it -v /path/on/host/:/app -p 5000:5000 python:3.6 bash # also binds host port to port inside container so it can be reached if an application responds on port 5000
docker build -f Dockerfile -t gvanhoy/flask_gunicorn . # builds an image from a file called Dockerfile and tags the image as gvanhoy/flask_gunicorn
docker-compose up # starts containers as described in a docker-compose.yml file in the same directory
docker-compose up -d # same thing, but keep the containers running as a daemon
```
