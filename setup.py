from setuptools import setup

setup(
    name='pylabel',
    version='0.0.1',
    description='PyLabel Demonstration',
    author='Garrett Vanhoy',
    author_email='gvanhoy@umd.edu',
    test_suite="test"
)
