FROM python:3.6

WORKDIR /app

COPY requirements.txt /app
RUN pip install -r requirements.txt

CMD ["gunicorn", "-w", "4", "--bind", "0.0.0.0:8000", "--reload", "wsgi:app"]
