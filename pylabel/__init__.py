from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from pylabel.config import Config


db = SQLAlchemy()
app = Flask(__name__)
login_manager = LoginManager()
nav = Nav()


def create_app(configuration):
    app.config.from_object(configuration)

    Bootstrap(app)

    from pylabel.accounts.routes import accounts
    app.register_blueprint(accounts, url_prefix='/users', static_folder='static')

    db.init_app(app)
    login_manager.login_view = 'accounts.login'
    login_manager.init_app(app)

    from pylabel.navigation import account_nav
    nav.init_app(app)

    with app.app_context():
        from pylabel.accounts.models import User
        db.create_all()

    @app.route("/")
    def hello():
        return "Hello World CI CD!"

    return app


if __name__ == "__main__":
    app.run()
