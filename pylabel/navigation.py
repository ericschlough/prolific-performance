from flask_nav.elements import Navbar, View, Separator
from pylabel import nav


@nav.navigation()
def account_nav():
    return Navbar(
        'PyLabel',
        View('Home', 'accounts.index'),
        View('Login', 'accounts.login'),
        View('Register', 'accounts.register'),
    )
