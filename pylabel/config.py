class Config:
    SECRET_KEY = 'super secret key'


class TestConfig(Config):
    pass
    # Database
    # SQLALCHEMY_DATABASE_URI = 'mysql://db/test'
    # SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'mysql://db/prod'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
