from flask import Blueprint, flash, url_for, redirect, render_template
from flask_login import login_user, login_required, current_user, logout_user
from werkzeug.security import check_password_hash, generate_password_hash
from pylabel.accounts.forms import LoginForm, RegisterForm
from pylabel import db
from pylabel.accounts.models import User

accounts = Blueprint('accounts', __name__, template_folder='templates')


@accounts.route('/')
def index():
    users = User.query.all()
    return render_template('index.html', users=users)


@accounts.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter(User.email == form.email.data).first()
        if user and check_password_hash(user.password, form.password.data):
            login_user(user, remember=True)
            flash('Logged in successfully.', 'success')
            return redirect(url_for("accounts.index"))

        flash('Invalid login.', 'danger')
        return redirect(url_for("accounts.login"))

    return render_template("login.html", form=form)


@accounts.route("/logout", methods=["GET"])
@login_required
def logout():
    """Logout the current user."""
    logout_user()
    return render_template("logout.html")


@accounts.route("/register", methods=("GET", "POST"))
def register():
    """Register a new user.
    Validates that the username is not already taken. Hashes the
    password for security.
    """
    form = RegisterForm()
    if form.validate_on_submit():
        user = User.query.filter(User.email == form.email.data).first()
        if user:
            flash('Email already taken.', 'info')
            return render_template("register.html", form=form)

        user = User(
            username=form.username.data,
            email=form.email.data,
            password=generate_password_hash(form.password.data)
        )
        db.session.add(user)
        db.session.commit()
        flash('New account created successfully.', 'success')
        return render_template("index.html")

    return render_template("register.html", form=form)
