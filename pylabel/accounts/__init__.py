from pylabel import login_manager


@login_manager.user_loader
def load_user(user_id):
    from pylabel.accounts.models import User
    return User.query.get(user_id)
