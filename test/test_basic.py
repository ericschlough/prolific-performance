import unittest
from pylabel import create_app
from pylabel.config import TestConfig


class BasicTest(unittest.TestCase):
    def test_main_page(self):
        app = create_app(TestConfig)
        self.app = app.test_client()
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
